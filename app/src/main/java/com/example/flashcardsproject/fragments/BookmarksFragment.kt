package com.example.flashcardsproject.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.flashcardsproject.R
import com.example.flashcardsproject.activities.MainActivity
import com.example.flashcardsproject.activities.PackageAccessActivity
import com.example.flashcardsproject.utils.Package
import com.example.flashcardsproject.utils.PackagesAdaptor

class BookmarksFragment : Fragment() {

    private lateinit var packages: ArrayList<Package>
    private lateinit var adapter: ArrayAdapter<Package>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_bookmarks, container, false)

        packages = ArrayList()
        adapter = PackagesAdaptor(
            this.context as Context,
            R.layout.package_layout,
            packages
        )
        val listView: ListView = root.findViewById(R.id.bookmarks_list)
        listView.adapter = adapter
        registerForContextMenu(listView)
        listView.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent(context, PackageAccessActivity::class.java).apply {
                putExtra("OWNER", packages[position].owner)
                putExtra("NAME", packages[position].name)
            }
            startActivity(intent)
        }

        loadBookmarks()
        return root
    }

    private fun loadBookmarks() {
        MainActivity.db.collection("packages")
            .whereEqualTo("share", true)
            .orderBy("name")
            .get()
            .addOnSuccessListener {
                it.forEach {
                    if (MainActivity.bookmarks.contains(it.id)) {
                        adapter.add(
                            Package(
                                it.get("owner") as String,
                                it.get("name") as String
                            )
                        )
                    }
                }
            }
    }
}