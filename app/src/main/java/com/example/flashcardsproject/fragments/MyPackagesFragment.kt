package com.example.flashcardsproject.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.*
import android.widget.*
import android.widget.AdapterView.AdapterContextMenuInfo
import androidx.fragment.app.Fragment
import com.example.flashcardsproject.R
import com.example.flashcardsproject.activities.MainActivity
import com.example.flashcardsproject.activities.PackageAccessActivity
import com.example.flashcardsproject.utils.Package
import com.example.flashcardsproject.utils.PackagesAdaptor
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query

class MyPackagesFragment : Fragment() {

    private lateinit var packages: ArrayList<Package>
    private lateinit var adapter: ArrayAdapter<Package>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_mypackages, container, false)

        packages = ArrayList()
        adapter = PackagesAdaptor(
            this.context as Context,
            R.layout.package_layout,
            packages
        )

        val fab: FloatingActionButton = root.findViewById(R.id.mypackages_fab)
        val listView: ListView = root.findViewById(R.id.mypackages_list)

        //ListView
        listView.adapter = adapter
        registerForContextMenu(listView)
        listView.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent(context, PackageAccessActivity::class.java).apply {
                putExtra("OWNER", packages[position].owner)
                putExtra("NAME", packages[position].name)
            }
            startActivity(intent)
        }

        //Floating action point
        fab.setOnClickListener {
            val inflater: LayoutInflater =
                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater?.inflate(R.layout.newpackage_layout, null)
            val name: EditText = view.findViewById(R.id.newpackage_name)
            val description: EditText = view.findViewById(R.id.newpackage_description)
            val share: CheckBox = view.findViewById(R.id.newpackage_share)

            //Alert dialog
            AlertDialog.Builder(context as Context)
                .setTitle("New package")
                .setView(view)
                .setPositiveButton("Ok") { _, _ ->
                    newPackage(
                        name.text.toString(),
                        description.text.toString(),
                        share.isChecked
                    )
                }
                .setNegativeButton("Cancel", null)
                .show()
        }
        loadMyPackages()
        return root
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        if (v?.id == R.id.mypackages_list) {
            menu!!.add("Rename")
            menu.add("Delete")
        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val info = item!!.menuInfo as AdapterContextMenuInfo
        if (item.title.equals("Rename")) {
            val et = EditText(context)
            et.setText(packages[info.position].name)
            et.filters = arrayOf(InputFilter.LengthFilter(20))
            AlertDialog.Builder(context as Context)
                .setTitle("New name")
                .setView(et)
                .setPositiveButton("Ok") { _, _ ->
                    renamePackage(packages[info.position], et.text.toString())
                }
                .setNegativeButton("Cancel", null)
                .show()
        } else if (item.title.equals("Delete")) {
            deletePackage(packages[info.position])
            adapter.remove(packages[info.position])
        }
        return true
    }

    private fun loadMyPackages() {
        val user = MainActivity.auth.currentUser?.email
        MainActivity.db.collection("packages")
            .whereEqualTo("owner", user)
            .orderBy("timestamp", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener {
                for (doc in it) {
                    val owner = doc.get("owner") as String
                    val name = doc.get("name") as String
                    adapter.add(
                        Package(
                            owner,
                            name
                        )
                    )
                }
            }
    }

    private fun newPackage(name: String, description: String, share: Boolean) {
        if (name.isEmpty()) {
            Toast.makeText(context, "Need a name", Toast.LENGTH_SHORT).show()
        } else {
            val pkg: MutableMap<String, Any> = mutableMapOf()
            val user = MainActivity.auth.currentUser?.email
            if (user != null) {
                pkg["owner"] = user
                pkg["name"] = name
                pkg["description"] = description
                pkg["share"] = share
                pkg["timestamp"] = FieldValue.serverTimestamp()

                MainActivity.db.collection("packages")
                    .whereEqualTo("owner", user)
                    .whereEqualTo("name", name)
                    .get()
                    .addOnSuccessListener { documents ->
                        if (documents.isEmpty) {
                            MainActivity.db.collection("packages")
                                .add(pkg)
                            adapter.add(
                                Package(
                                    user,
                                    name
                                )
                            )
                        } else
                            Toast.makeText(
                                context,
                                "The package already exists",
                                Toast.LENGTH_SHORT
                            ).show()
                    }
            }
        }
    }

    private fun deletePackage(p: Package) {
        MainActivity.db.collection("packages")
            .whereEqualTo("name", p.name)
            .whereEqualTo("owner", p.owner)
            .get()
            .addOnSuccessListener {
                it.forEach { doc -> doc.reference.delete() }
            }
    }

    private fun renamePackage(p: Package, newname: String) {
        if (newname.isEmpty()) {
            Toast.makeText(context, "Need a name", Toast.LENGTH_SHORT).show()
        } else {
            if (p.name != newname) {
                MainActivity.db.collection("packages")
                    .whereEqualTo("owner", p.owner)
                    .whereEqualTo("name", newname)
                    .get()
                    .addOnSuccessListener { documents ->
                        if (documents.isEmpty) {
                            MainActivity.db.collection("packages")
                                .whereEqualTo("owner", p.owner)
                                .whereEqualTo("name", p.name)
                                .get()
                                .addOnSuccessListener {
                                    it.forEach { doc ->
                                        doc.reference.update("name", newname)
                                    }
                                    p.name = newname
                                    adapter.notifyDataSetChanged()
                                }
                        } else {
                            Toast.makeText(
                                context,
                                "The name is already used",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
    }
}