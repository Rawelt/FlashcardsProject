package com.example.flashcardsproject.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.flashcardsproject.R
import com.example.flashcardsproject.activities.MainActivity
import com.example.flashcardsproject.activities.PackageAccessActivity
import com.example.flashcardsproject.utils.Package
import com.example.flashcardsproject.utils.PackagesAdaptor
import com.google.firebase.firestore.Query

class SearchFragment : Fragment() {

    private lateinit var packages: ArrayList<Package>
    private lateinit var adapter: ArrayAdapter<Package>
    private lateinit var et_name: EditText
    private lateinit var et_owner: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)
        et_name = root.findViewById(R.id.search_name)
        et_owner = root.findViewById(R.id.search_owner)
        packages = ArrayList()
        adapter = PackagesAdaptor(context as Context, R.layout.package_layout, packages)

        //ListView
        val listView: ListView = root.findViewById(R.id.search_list)
        listView.adapter = adapter
        listView.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent(context, PackageAccessActivity::class.java).apply {
                putExtra("OWNER", packages[position].owner)
                putExtra("NAME", packages[position].name)
            }
            startActivity(intent)
        }

        //Buttons
        val btn_search: ImageView = root.findViewById(R.id.search_btn_search)
        btn_search.setOnClickListener { onSearchButtonClick(it) }
        val btn_name_clear: ImageView = root.findViewById(R.id.search_btn_name_clear)
        btn_name_clear.setOnClickListener { onNameClearButtonClick(it) }
        val btn_owner_clear: ImageView = root.findViewById(R.id.search_btn_owner_clear)
        btn_owner_clear.setOnClickListener { onOwnerClearButtonClick(it) }

        return root
    }

    fun onNameClearButtonClick(v: View) {
        et_name.text.clear()
    }

    fun onOwnerClearButtonClick(v: View) {
        et_owner.text.clear()
    }

    fun onSearchButtonClick(v: View) {
        val haveName = et_name.text.toString().isNotEmpty()
        val haveOwner = et_owner.text.toString().isNotEmpty()
        Log.i("SEARCH", "name ${et_name.text.toString()}, owner ${et_owner.text.toString()}")
        if (haveName || haveOwner) {
            val col = MainActivity.db.collection("packages")
            if (haveName && haveOwner) {
                col.whereEqualTo("share", true)
                    .whereEqualTo("name", et_name.text.toString())
                    .whereEqualTo("owner", et_owner.text.toString())
                    .get()
                    .addOnSuccessListener {
                        packages.clear()
                        it.forEach { doc ->
                            val owner = doc.get("owner") as String
                            val name = doc.get("name") as String
                            packages.add(
                                Package(
                                    owner,
                                    name
                                )
                            )
                        }
                        if (packages.isEmpty()) {
                            Toast.makeText(
                                context,
                                "No packages corresponding",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        adapter.notifyDataSetChanged()
                    }
            } else if (haveName) {
                col.whereEqualTo("share", true)
                    .whereEqualTo("name", et_name.text.toString())
                    .get()
                    .addOnSuccessListener {
                        packages.clear()
                        it.forEach { doc ->
                            val owner = doc.get("owner") as String
                            val name = doc.get("name") as String
                            packages.add(
                                Package(
                                    owner,
                                    name
                                )
                            )
                        }
                        if (packages.isEmpty()) {
                            Toast.makeText(
                                context,
                                "No packages corresponding",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        adapter.notifyDataSetChanged()
                    }

            } else if (haveOwner) {
                col.whereEqualTo("share", true)
                    .whereEqualTo("owner", et_owner.text.toString())
                    .get()
                    .addOnSuccessListener {
                        packages.clear()
                        it.forEach { doc ->
                            val owner = doc.get("owner") as String
                            val name = doc.get("name") as String
                            packages.add(
                                Package(
                                    owner,
                                    name
                                )
                            )
                        }
                        if (packages.isEmpty()) {
                            Toast.makeText(
                                context,
                                "No packages corresponding",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        adapter.notifyDataSetChanged()
                    }
            }
            et_name.text.clear()
            et_owner.text.clear()
        }

        fun onSearchButtonClick(v: View) {
            val haveName = et_name.text.toString().isNotEmpty()
            val haveOwner = et_owner.text.toString().isNotEmpty()
            Log.i("SEARCH", "name ${et_name.text.toString()}, owner ${et_owner.text.toString()}")
            if (haveName || haveOwner) {
                var col = MainActivity.db.collection("packages")
                    .whereEqualTo("share", true)
                if (haveName)
                    col = col.whereEqualTo("name", et_name.text.toString())
                if (haveOwner)
                    col = col.whereEqualTo("owner", et_owner.text.toString())

                col.orderBy("timestamp", Query.Direction.DESCENDING)
                    .get()
                    .addOnSuccessListener {
                        packages.clear()
                        if(it.isEmpty){
                            Toast.makeText(
                                context,
                                "No packages corresponding",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            it.forEach { doc ->
                                val owner = doc.get("owner") as String
                                val name = doc.get("name") as String
                                packages.add(
                                    Package(
                                        owner,
                                        name
                                    )
                                )
                            }
                        }
                        adapter.notifyDataSetChanged()
                    }
                et_name.text.clear()
                et_owner.text.clear()
            }
        }
    }
}