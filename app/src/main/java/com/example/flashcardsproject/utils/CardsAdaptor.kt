package com.example.flashcardsproject.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.flashcardsproject.R

class CardsAdaptor(context: Context, resource: Int, objects: MutableList<Card>) :
    ArrayAdapter<Card>(context, resource, objects) {

    private val resource = resource
    private val objects = objects

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val retView: View =
            convertView ?: LayoutInflater.from(context).inflate(resource, parent, false)
        val holder: CardViewHolder
        if (retView.tag == null) {
            holder =
                CardViewHolder(
                    retView.findViewById(R.id.card_front),
                    retView.findViewById(R.id.card_back)
                )
            retView.tag = holder
        } else {
            holder = retView.tag as CardViewHolder
        }
        holder.front.text = objects[position].front
        holder.back.text = objects[position].back
        return retView
    }

    private class CardViewHolder(var front: TextView,var back: TextView)
}