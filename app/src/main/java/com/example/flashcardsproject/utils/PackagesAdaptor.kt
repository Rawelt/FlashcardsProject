package com.example.flashcardsproject.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.flashcardsproject.R

class PackagesAdaptor(context: Context, resource: Int, objects: MutableList<Package>) :
    ArrayAdapter<Package>(context, resource, objects) {

    private val resource = resource
    private val objects = objects

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val retView: View =
            convertView ?: LayoutInflater.from(context).inflate(resource, parent, false)
        val holder: PackageViewHolder
        if (retView.tag == null) {
            holder =
                PackageViewHolder(
                    retView.findViewById(R.id.package_name),
                    retView.findViewById(R.id.package_owner)
                )
            retView.tag = holder
        } else {
            holder = retView.tag as PackageViewHolder
        }
        holder.name.text = objects[position].name
        holder.owner.text = "Owner: ${objects[position].owner}"
        return retView
    }


    private class PackageViewHolder(
        var name: TextView,
        var owner: TextView
    )

}