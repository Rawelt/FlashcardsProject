package com.example.flashcardsproject.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import com.example.flashcardsproject.R
import com.example.flashcardsproject.utils.Card
import com.example.flashcardsproject.utils.CardsAdaptor
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query

class PackageAccessActivity : AppCompatActivity() {

    private lateinit var docRef: DocumentReference
    private lateinit var owner: String
    private lateinit var name: String

    private lateinit var tv_description: TextView

    private lateinit var cards: ArrayList<Card>
    private lateinit var adapter: ArrayAdapter<Card>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package_access)

        name = intent.getStringExtra("NAME")
        owner = intent.getStringExtra("OWNER")

        MainActivity.db.collection("packages")
            .whereEqualTo("owner", owner)
            .whereEqualTo("name", name)
            .get()
            .addOnSuccessListener {
                if (it.isEmpty) {
                    finish()
                } else {
                    for (doc in it) {
                        docRef = doc.reference //Only 1
                        setInfo(
                            doc.get("description") as String,
                            doc.get("share") as Boolean
                        )
                    }
                    run()
                }
            }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        if (v?.id == R.id.package_access_list) {
            menu!!.add("Delete")
        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val info = item!!.menuInfo as AdapterView.AdapterContextMenuInfo
        if (item.title.equals("Delete")) {
            deleteCard(cards[info.position])
            adapter.remove(cards[info.position])
        }
        return true
    }

    private fun setInfo(descr: String, share: Boolean) {
        title = "Package : $name"
        val tv_owner: TextView = findViewById(R.id.package_access_owner)
        tv_owner.text = "Owner: $owner"
        tv_description = findViewById(R.id.package_access_description)
        tv_description.text = descr

        //Set share or bookmark button
        val cb_share: CheckBox = findViewById(R.id.package_access_share)
        val img_bookmark: ImageView = findViewById(R.id.package_access_bookmark)
        if (!isOwner()) {
            findViewById<LinearLayout>(R.id.package_access_share_box).visibility = View.GONE
            img_bookmark.visibility = View.VISIBLE
            if (MainActivity.bookmarks.contains(docRef.id)) {
                img_bookmark.setImageResource(R.drawable.ic_star)
            } else {
                img_bookmark.setImageResource(R.drawable.ic_star_border)
            }
        } else {
            findViewById<LinearLayout>(R.id.package_access_share_box).visibility = View.VISIBLE
            img_bookmark.visibility = View.GONE
            cb_share.isChecked = share
        }
    }

    private fun run() {
        cards = ArrayList()
        adapter = CardsAdaptor(this, R.layout.card_layout, cards)
        val listView: ListView = findViewById(R.id.package_access_list)
        listView.adapter = adapter

        //OnClickListener of the ListView
        listView.setOnItemClickListener { _, _, position, _ ->
            val inflater: LayoutInflater =
                getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            if (isOwner()) {
                val view = inflater.inflate(R.layout.newcard_layout, null)
                val ed_front: EditText = view.findViewById(R.id.newcard_front)
                val ed_back: EditText = view.findViewById(R.id.newcard_back)
                ed_front.setText(cards[position].front)
                ed_back.setText(cards[position].back)
                AlertDialog.Builder(this)
                    .setTitle("Modify card")
                    .setView(view)
                    .setPositiveButton("Ok") { _, _ ->
                        updateCard(
                            cards[position].front,
                            cards[position].back,
                            ed_front.text.toString(),
                            ed_back.text.toString(),
                            position
                        )
                    }
                    .setNegativeButton("Cancel", null)
                    .show()
            } else {
                val view = inflater.inflate(R.layout.showcard_layout, null)
                val tv_front: TextView = view.findViewById(R.id.showcard_front)
                val tv_back: TextView = view.findViewById(R.id.showcard_back)
                tv_front.text = cards[position].front
                tv_back.text = cards[position].back
                AlertDialog.Builder(this)
                    .setTitle("Card")
                    .setView(view)
                    .setPositiveButton("Ok", null)
                    .show()
            }
        }

        if (isOwner()) registerForContextMenu(listView)
        setListeners()
        loadCards()
    }

    private fun setListeners() {
        val fab: FloatingActionButton = findViewById(R.id.package_access_fab)
        if (isOwner()) {
            //Floating Point Action when we can create new cards
            fab.show()
            fab.setOnClickListener {
                val inflater: LayoutInflater =
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val view = inflater.inflate(R.layout.newcard_layout, null)
                val ed_front: EditText = view.findViewById(R.id.newcard_front)
                val ed_back: EditText = view.findViewById(R.id.newcard_back)
                AlertDialog.Builder(this)
                    .setTitle("New card")
                    .setView(view)
                    .setPositiveButton("Ok") { _, _ ->
                        newCard(
                            ed_front.text.toString(),
                            ed_back.text.toString()
                        )
                    }
                    .setNegativeButton("Cancel", null)
                    .show()
            }

            //Description
            tv_description.setOnClickListener {
                val et = EditText(this)
                et.setText(tv_description.text)
                et.filters = arrayOf(InputFilter.LengthFilter(300))
                et.gravity = Gravity.TOP
                AlertDialog.Builder(this)
                    .setTitle("New description")
                    .setView(et)
                    .setPositiveButton("Ok") { _, _ ->
                        docRef.update("description", et.text.toString())
                        tv_description.setText(et.text.toString())
                    }
                    .setNegativeButton("Cancel", null)
                    .show()
            }
        } else {
            fab.hide()

            //Show the description
            tv_description.setOnClickListener {
                val tv = TextView(this)
                tv.setText(tv_description.text)
                tv.gravity = Gravity.TOP
                tv.setPadding(50)
                AlertDialog.Builder(this)
                    .setTitle("Description")
                    .setView(tv)
                    .setPositiveButton("Ok", null)
                    .show()
            }
        }
    }


    private fun loadCards() {
        docRef.collection("cards")
            .orderBy("timestamp", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener {
                for (doc in it) {
                    adapter.add(
                        Card(
                            doc.get("front") as String,
                            doc.get("back") as String
                        )
                    )
                }
            }
    }

    private fun newCard(front: String, back: String) {
        if (front.isEmpty() || back.isEmpty()) {
            Toast.makeText(this, "A side is empty", Toast.LENGTH_SHORT).show()
        } else {
            docRef.collection("cards")
                .whereEqualTo("front", front)
                .whereEqualTo("back", back)
                .get()
                .addOnSuccessListener {
                    if (it.isEmpty) {
                        docRef.collection("cards")
                            .add(
                                mapOf(
                                    "front" to front,
                                    "back" to back,
                                    "timestamp" to FieldValue.serverTimestamp()
                                )
                            )
                            .addOnSuccessListener { adapter.add(Card(front, back)) }
                    } else {
                        Toast.makeText(
                            this,
                            "The card already exists",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    private fun updateCard(
        oldFront: String, oldBack: String,
        newFront: String, newBack: String,
        position: Int
    ) {
        if (newFront.isEmpty() || newBack.isEmpty()) {
            Toast.makeText(this, "A side is empty", Toast.LENGTH_SHORT).show()
        } else {
            if (!oldFront.equals(newFront) || !oldBack.equals(newBack)) {
                docRef.collection("cards")
                    .whereEqualTo("front", newFront)
                    .whereEqualTo("back", newBack)
                    .get()
                    .addOnSuccessListener {
                        if (it.isEmpty) {
                            docRef.collection("cards")
                                .whereEqualTo("front", oldFront)
                                .whereEqualTo("back", oldBack)
                                .get()
                                .addOnSuccessListener { docs ->
                                    docs.forEach { doc ->
                                        doc.reference.update(
                                            mapOf(
                                                "front" to newFront,
                                                "back" to newBack
                                            )
                                        )
                                            .addOnSuccessListener {
                                                cards[position].front = newFront
                                                cards[position].back = newBack
                                                adapter.notifyDataSetChanged()
                                            }
                                    }
                                }
                        } else {
                            Toast.makeText(
                                this,
                                "The card already exists",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
    }

    private fun deleteCard(c: Card) {
        docRef.collection("cards")
            .whereEqualTo("front", c.front)
            .whereEqualTo("back", c.back)
            .get()
            .addOnSuccessListener {
                it.forEach { doc -> doc.reference.delete() } //Only 1
            }
    }


    private fun isOwner(): Boolean {
        val user = MainActivity.auth.currentUser?.email
        return user.equals(owner)
    }

    fun onTrainButtonClick(v: View) {
        if (cards.isNotEmpty()) {
            val intent = Intent(this, TrainingActivity::class.java).apply {
                putExtra("OWNER", owner)
                putExtra("NAME", name)
            }
            startActivity(intent)
        }
    }

    fun onShareClick(v: View) {
        val cb = v as CheckBox
        docRef.update("share", cb.isChecked)
    }

    fun onBookmarkClick(v: View) {
        val img_bookmark = v as ImageView
        val user = MainActivity.auth.currentUser?.email
        if(user != null) {
            if (MainActivity.bookmarks.contains(docRef.id)) {
                MainActivity.db.collection("bookmarks")
                    .document(user)
                    .update("bookmarks", FieldValue.arrayRemove(docRef.id))
                    .addOnSuccessListener {
                        MainActivity.bookmarks.remove(docRef.id)
                        img_bookmark.setImageResource(R.drawable.ic_star_border)
                    }
            } else {
                MainActivity.db.collection("bookmarks")
                    .document(user)
                    .update("bookmarks", FieldValue.arrayUnion(docRef.id))
                    .addOnSuccessListener {
                        MainActivity.bookmarks.add(docRef.id)
                        img_bookmark.setImageResource(R.drawable.ic_star)
                    }
            }
        }
    }
}
