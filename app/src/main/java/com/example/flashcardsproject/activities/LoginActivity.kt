package com.example.flashcardsproject.activities

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.flashcardsproject.R
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var email_field: EditText
    private lateinit var password_field: EditText
    private lateinit var error_msg: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        email_field = findViewById(R.id.field_email)
        password_field = findViewById(R.id.field_password)
        error_msg = findViewById(R.id.error_msg)

        val link: TextView = findViewById(R.id.link_signup)
        link.paintFlags = link.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    fun btnLogInOnClick(v: View) {
        auth.signInWithEmailAndPassword(email_field.text.toString(), password_field.text.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    error_msg.text = "Wrong e-mail or wrong password"
                    error_msg.visibility = View.VISIBLE
                }
            }
    }

    fun signUpOnClick(v: View) {
        val intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
        finish()
    }
}
