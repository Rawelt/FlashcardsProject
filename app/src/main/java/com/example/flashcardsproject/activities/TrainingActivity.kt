package com.example.flashcardsproject.activities

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.flashcardsproject.R
import com.example.flashcardsproject.utils.Card

class TrainingActivity : AppCompatActivity() {

    private lateinit var owner: String
    private lateinit var name: String
    private lateinit var cards: ArrayList<Card>
    private lateinit var badCards: ArrayList<Card>

    private lateinit var tv_remaining:TextView

    private lateinit var tv_front: TextView
    private lateinit var tv_back: TextView

    private lateinit var btn_show: TextView
    private lateinit var box_btn: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training)

        name = intent.getStringExtra("NAME")
        owner = intent.getStringExtra("OWNER")
        cards = ArrayList()
        badCards = ArrayList()

        MainActivity.db.collection("packages")
            .whereEqualTo("owner", owner)
            .whereEqualTo("name", name)
            .get()
            .addOnSuccessListener {
                if (it.isEmpty) {
                    finish()
                } else {
                    it.forEach { doc ->
                        doc.reference.collection("cards")
                            .get()
                            .addOnSuccessListener {
                                it.forEach {
                                    cards.add(
                                        Card(
                                            it.get("front") as String,
                                            it.get("back") as String
                                        )
                                    )
                                }
                                run()
                            }
                    }
                }
            }
    }


    private fun run() {
        //Get views
        tv_remaining = findViewById(R.id.training_remaining)
        tv_front = findViewById(R.id.training_front)
        tv_back = findViewById(R.id.training_back)
        btn_show = findViewById(R.id.training_show_btn)
        box_btn = findViewById(R.id.training_btn_box)

        //Shuffle
        cards.shuffle()

        //Show first card
        nextCard()
    }

    fun onShowAnswerClick(v: View) {
        tv_back.visibility = View.VISIBLE
        btn_show.visibility = View.INVISIBLE
        box_btn.visibility = View.VISIBLE
    }

    fun onBadButtonClick(v: View) {
        badCards.add(cards.removeAt(0))
        nextCard()
    }

    fun onGoodButtonClick(v: View) {
        cards.removeAt(0)
        nextCard()
    }

    private fun nextCard() {
        if (cards.isEmpty()) {
            if (badCards.isEmpty()) {
                finish()
                return
            } else {
                cards = badCards
                cards.shuffle()
                badCards = ArrayList()
            }
        }
        val card = cards.get(0)
        tv_remaining.text = "${cards.size + badCards.size}"
        tv_front.text = card.front
        tv_back.text = card.back
        tv_back.visibility = View.INVISIBLE
        btn_show.visibility = View.VISIBLE
        box_btn.visibility = View.INVISIBLE
    }
}